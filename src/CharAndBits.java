public class CharAndBits implements Comparable<CharAndBits>{
	
	private char theChar;
	private char toAscii;
	private int intValue;
	private String binaryStr;
	private char[] binaryChar;
	private int[] binaryInt;
	
	public CharAndBits(char theChar) {
		this.theChar = theChar;
		if (isPrintable())
			toAscii = theChar;
		else
			toAscii = '.';
		
		intValue = theChar;
		intValue &= 0xFF;
		
		binaryChar = new char[8];
		binaryInt = new int[8];
		int temp = intValue;
		for (int i = 7; i >= 0; i--) {
			binaryInt[i] = temp & 1;
			binaryChar[i] = (char) binaryInt[i];
			binaryChar[i] += '0';
			temp >>>= 1;
		}
		binaryStr = new String(binaryChar);
	}
	
	public boolean isPrintable() {
		return !Character.isISOControl(theChar);
	}
	
	public String toBinaryString() {
		return binaryStr;
	}
	
	public static String toAsciiString(String encoded) {
		CharAndBits[] array = newCharAndBitsArray(encoded);
		char[] digit = new char[encoded.length()];
		for (int i = 0; i < encoded.length(); i++)
			digit[i] = array[i].toAscii;
		return new String(digit);
	}
	
	public char getPrintableChar() {
		return toAscii;
	}
	
	public int getIntValue() {
		return intValue;
	}
	
	public char[] getBinaryChar() {
		return binaryChar;
	}
	
	public boolean equals(CharAndBits other) {
		return compareTo(other) == 0;
	}
	
	public int compareTo(CharAndBits other) {
		return intValue - other.intValue;
	}
	
	public static CharAndBits[] newCharAndBitsArray(String str) {
		if (str == null)
			return new CharAndBits[0];
		char[] digit = str.toCharArray();
		CharAndBits[] array = new CharAndBits[digit.length];
		for (int i = 0; i < digit.length; i++)
			array[i] = new CharAndBits(digit[i]);
		return array;
	}
	
	public static String getMsgString(CharAndBits[] array) {
		StringBuilder sb = new StringBuilder(array.length);
		for (CharAndBits ea : array)
			sb.append(ea.toAscii);
		return sb.toString();
	}
	
	public static String xorArray(CharAndBits[] msg, CharAndBits[] key) {
		if (msg == null || msg.length == 0)
			return "";
		int msgLen = msg.length;
		StringBuilder sb = new StringBuilder(msgLen);
		
		if (key == null || key.length == 0) {
			for (CharAndBits ea : msg)
				sb.append(ea.theChar);
			return sb.toString();
		}
		int keyLen = key.length;
		for (int i = 0; i < msgLen; i++) {
			int val = msg[i].intValue ^ key[i % keyLen].intValue;
			sb.append((char) val);
		}
		return sb.toString();
	}
	
	public static String toBinaryString(String str) {
		return toBinaryString(str.toCharArray());
	}
	
	public static String toBinaryString(char[] digit) {
		StringBuilder sb = new StringBuilder(digit.length * 9);
		for (char c : digit) {
			CharAndBits ea = new CharAndBits(c);
			sb.append(ea.toBinaryString());
			sb.append(' ');
	}
	return sb.toString();
	}
}