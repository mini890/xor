import java.util.Scanner;

public class Xor {
	
	private String key;
	
	public Xor(String key) {
		setKey(key);
	}
	
	public void setKey(String key) {
		if (key == null)
			key = "";
		this.key = key;
	}
	
	public String encodeDecode(String msg) {
		if (msg == null || msg.length() == 0)
			return msg;
		if (key.length() == 0)
			return msg;
		CharAndBits[] m = CharAndBits.newCharAndBitsArray(msg); 
		CharAndBits[] k = CharAndBits.newCharAndBitsArray(key);
		String encodeDecodeValue = CharAndBits.xorArray(m, k);
		return encodeDecodeValue;
	}
	
	public String dupKey(int msgLen) {
		if (key.length() == 0)
			return "";
		String dup = key;
		while (dup.length() < msgLen)
			dup += key;
		return dup.substring(0, msgLen);
	}
	
	public static void main(String[] args) {
		
		String msg = "DreamInCode";
		String key = "mini890";
		Xor xor = new Xor(key);
		
		String dupKey = xor.dupKey(msg.length());
		System.out.println("The original message is: \"" + msg + "\" the key used will be \"" + dupKey + "\"");
		String msgInBin = CharAndBits.toBinaryString(msg);
		System.out.println(msgInBin);
		
		String keyInBin = CharAndBits.toBinaryString(dupKey);
		System.out.println(keyInBin);
		char[] dash = new char[msgInBin.length()];
		for (int i = 0; i < msgInBin.length(); i++)
			dash[i] = '-';
		System.out.println(new String(dash));
		
		String encoded = xor.encodeDecode(msg);
		String encodedInBinary = CharAndBits.toBinaryString(encoded);
		System.out.println(encodedInBinary);
		System.out.println("The encrypted message is: \"" + CharAndBits.toAsciiString(encoded) + "\"");
		
		System.out.println();
		System.out.println("The encoded message XORed with the key");
		System.out.println(encodedInBinary);
		System.out.println(keyInBin);
		System.out.println(new String(dash));
		
		String decoded = xor.encodeDecode(encoded);
		System.out.println(CharAndBits.toBinaryString(decoded));
		System.out.println("The decoded message is \"" + decoded + "\" is it the same as \"" + msg + "\": " + msg.equals(decoded));
		
		Scanner scan = new Scanner(System.in);
		String userKey;
		System.out.println();
		do {
			System.out.println("Enter the key to use: ");
			userKey = scan.nextLine();
		} while (userKey.length() == 0);
		
		Xor userXor = new Xor(userKey);
		System.out.println("Enter message to encode: ");
		String userMsg = scan.nextLine();
		
		String userDupKey =  userXor.dupKey(userMsg.length());
		System.out.println("The original message is: \"" + userMsg + "\" the key used will be \"" + userDupKey + "\"");
		String userMsgInBin = CharAndBits.toBinaryString(userMsg);
		System.out.println(userMsgInBin);
		
		String userKeyInBin = CharAndBits.toBinaryString(userDupKey);
		System.out.println(userKeyInBin);
		char[] userDash = new char[userMsgInBin.length()];
		for(int i = 0; i < userMsgInBin.length(); i++)
			userDash[i] = '-';
		System.out.println(new String(userDash));
		
		String userEncoded = userXor.encodeDecode(userMsg);
		String userEncodedInBinary = CharAndBits.toBinaryString(userEncoded); 
		System.out.println(userEncodedInBinary);
		System.out.println("The encrypted message is: \"" + CharAndBits.toAsciiString(userEncoded) + "\"");
		
		System.out.println();
		System.out.println("The encoded message XORed with the key");
		System.out.println(userEncodedInBinary);			// encoded message
		System.out.println(userKeyInBin);					// key in binary
		System.out.println(new String(userDash));           // the ------------
		
		String userDecoded = userXor.encodeDecode(userEncoded);
		System.out.println(CharAndBits.toBinaryString(userDecoded));
		System.out.println("The decoded message is \"" + userDecoded + "\" is it the same as \"" + userMsg + "\": " + userMsg.equals(userDecoded));
		scan.close();
	}

}