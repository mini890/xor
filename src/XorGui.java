import java.awt.*;

import javax.swing.*;
import javax.swing.event.*;
import javax.swing.table.*;

/**
 * A GUI that use the Xor class to encode/decode messages
 * In this GUI we cannot prompt for a String to decode as XOring bits
 * in byte may generate characters that may (most probably) not be displayable
 * 
 * The GUI display the binary form of each letter in the message
 * their coded representaion after the XOR operation
 * This GUI is greatly inspired from the SwapAndRotateGui of Secret Code V tutorial
 * 
 * Note that if a letter of the message and the corresponding letter of the key are equal
 * the encrypted letter is 00000000 
 * If message is ABC and key 123 the encrypted versions should be the same :-)
 */
public class XorGui extends JFrame {

	private static final long serialVersionUID = 1L;

	// the Xor class to encode/decode
	private Xor xor;

	// the key to use
	private JTextField keyText;
	// the letter of the key for every letter in the key
	private char[] longKey;
	// The message to encode
	private JTextField clearTextIn;

	// the original message
	private char[] msgChar = new char[0];
	// the encoded messages
	private char[] msgEncoded;
	// the decoded messages that should be the same as msgChar
	private char[] msgDecoded;

	// The JTable shown in the CENTER region
	private JTable table;
	private MyModel myModel;
	private TableColumnModel colModel;

	// Its panel
	private JPanel centerPanel;
	// the first column of the jTable
	private static final String[] firstCol = {"Message", "Msg Bin", "Key Bin", "XOR Encoded", "Encoded Ascii", "Key Bin", "XOR Decoded", "Decoded Ascii"};
	// mnemonic for more descriptive values in the AbstractModel
	private static final int ORIG = 0, ORIG_BIN = 1, KEY1_BIN = 2, CRYPTED_BIN = 3, CRYPTED = 4, KEY2_BIN = 5, DECRYPTED_BIN = 6, DECRYPTED = 7;

	/**
	 * Constructor
	 */
	XorGui() {
		super("XOR encoding/decoding");
		// we will use a BorderLayout to store our GUI component
		setLayout(new BorderLayout());

		// the Xor object init the key to ""
		xor = new Xor("");
		longKey = xor.dupKey(0).toCharArray();
		// the Listener the key changes
		DocumentListener dc = new KeyListener();
		// The NORTH region will contain a JPanel where the key and the message can be entererd
		JPanel north = new JPanel(new GridLayout(5, 1, 2, 2));
		// the key
		north.add(createCenteredLabel("The Key"));
		keyText = new JTextField(50);
		keyText.getDocument().addDocumentListener(dc);
		north.add(keyText);
		// the message
		north.add(createCenteredLabel("Enter the message to encode here"));
		clearTextIn = new JTextField(50);
		clearTextIn.getDocument().addDocumentListener(new ClearListener());
		north.add(clearTextIn);
		// a gap
		north.add(new JLabel(" "));
		// add this panel to the top of the screen
		add(north, BorderLayout.NORTH);

		// in the CENTER region of the frame we will put a JTable containing all the 
		// encrypted/decripted bits
		myModel = new MyModel();
		table = new JTable(myModel);
		table.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		colModel = table.getColumnModel();

		centerPanel = new JPanel(new BorderLayout());
		centerPanel.add(new JScrollPane(table), BorderLayout.CENTER);
		add(centerPanel, BorderLayout.CENTER);

		// standard operation to show the JFrame
		this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(30, 30, 700, 320);
		setVisible(true);

		// to set the size of column 1
		updateStringToEncode();
	}

	/**
	 * A method to create a JLabel with foreground color Blue and with text centered
	 */
	private JLabel createCenteredLabel(String text) {
		JLabel label = new JLabel(text);
		label.setHorizontalAlignment(SwingConstants.CENTER);
		label.setForeground(Color.BLUE);
		return label;
	}

	/**
	 * The key has changed 
	 */
	private void updateKeyString() {
		// update key in the Xor object from the key JTextField
		xor.setKey(keyText.getText());
		// update the encoding process
		updateStringToEncode();
	}

	/**
	 * To update the string to be coded
	 */
	private void updateStringToEncode() {
		// get the text of the message to encode from the JTextField
		String line = clearTextIn.getText();
		// make the char[] array out of it to be displayed in the JTable
		msgChar = line.toCharArray();
		// generate (for display purpose only) the key that will be used for every character
		longKey = xor.dupKey(line.length()).toCharArray();
		// build the encrypted message
		String encoded = xor.encodeDecode(line);
		// in a char[] for display purpose
		msgEncoded = encoded.toCharArray();
		// build the decrypted char[]
		msgDecoded = xor.encodeDecode(encoded).toCharArray();
		// inform the model that the table contain changed 
		myModel.fireTableStructureChanged();
		myModel.fireTableDataChanged();
		// set the size of the column when a new column is added lets set it's size
		int actualColumnCount = msgChar.length + 1;
		for(int i = 0; i < actualColumnCount; i++) {
			TableColumn tc = colModel.getColumn(i); 
			tc.setPreferredWidth(100);
			tc.setMinWidth(100);
		}
	}

	/**
	 * To start the GUI
	 */
	public static void main(String[] args) {
		new XorGui();
	}

	/**
	 * A listener to be informed whenever the JTextField of the clear text is changed
	 */
	private class ClearListener implements DocumentListener {
		@Override
		public void changedUpdate(DocumentEvent arg0) {
			updateStringToEncode();
		}
		@Override
		public void insertUpdate(DocumentEvent arg0) {
			updateStringToEncode();
		}
		@Override
		public void removeUpdate(DocumentEvent arg0) {
			updateStringToEncode();
		}
	}

	/**
	 * A listener to be informed whenever the JTextField of the SWAP or ROTATE key is changed
	 */
	private class KeyListener implements DocumentListener {
		@Override
		public void changedUpdate(DocumentEvent arg0) {
			updateKeyString();
		}
		@Override
		public void insertUpdate(DocumentEvent arg0) {
			updateKeyString();
		}
		@Override
		public void removeUpdate(DocumentEvent arg0) {
			updateKeyString();
		}
	}

	/** 
	 * A class that extends AbstractTableModel to povide the binary representation
	 * of every cell of the JTable in the center panel
	 */
	private class MyModel extends AbstractTableModel {

		private static final long serialVersionUID = 1L;

		// the number of columns is the length of the message + the first column
		public int getColumnCount() {
			return  msgChar.length + 1; 
		}

		// name of each colum (first one is empty)
		public String getColumnName(int column) {
			// if column 0 we return the hardcode "Steps"
			if(column == 0)
				return "Steps";
			
			// skip first column
			--column;
			// verify that we have data for this column
			if(column >= msgChar.length || column >= longKey.length)
				return "";

			// OK generate its title which is the letter of the key used
			return "#" + (column+1) + " Key: \"" + longKey[column] + "\"";
		}

		// return the row count
		public int getRowCount() {
			// it is the length of our first column
			return firstCol.length;
		}

		// the JTable want's to know what to print there
		public Object getValueAt(int row, int col) {
			// for the first column we just return the header
			if(col == 0)
				return firstCol[row];
			--col;
			// validate first the col it should be contained in the mesage
			if(col >= msgChar.length)
				return "";
			
			// the CharAndBits to display init to null we will check it later
			CharAndBits cab = null;

			// depending of the column display the correct text field
			switch(row) {
			    // the original message we juts return the character of that row
				case ORIG:
					return "      >" + msgChar[col] + "<";
				// the binary stransaltion of that letter
				case ORIG_BIN:
					cab = new CharAndBits(msgChar[col]);
					break;
	            // the binary representation of the key (same one at both row)
				case KEY1_BIN:
				case KEY2_BIN:
					if(col >= longKey.length)
						return "";
					cab = new CharAndBits(longKey[col]);
					break;		
				// the binary representation of the encrypted letter of the message
				case CRYPTED_BIN:
					cab = new CharAndBits(msgEncoded[col]);
					break;	
				// the ASCII representation (if it exists) of this letter of the message
				case CRYPTED:
					CharAndBits c = new CharAndBits(msgEncoded[col]);
					if(c.isPrintable())
						return "      >" + c.getPrintableChar() + "<";
					else
						return "      -";
				// the decrypted message back should be the same of ORIG_BIN
				case DECRYPTED_BIN:
					if(col >= msgDecoded.length)
						break;
					cab = new CharAndBits(msgDecoded[col]);
					break;
				// the letter of the message back into Ascii
				case DECRYPTED:
					return "      >" + msgDecoded[col] + "<";					
			}
			// common formatting the binary string followed by the printable version of the char
			// unless not defined yet due to the way the GUI refresh the JTable
			if(cab == null)
				return "";
			return cab.toBinaryString();
		}

	}

}